import Vue from 'vue'
import Vuex from 'vuex'
import user from './store/modules/user';
import album from './store/modules/album';
import photo from './store/modules/photo';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  modules: {
    user,
    album,
    photo
  }
})
