import Vue from 'vue'
import Router from 'vue-router'

const Users = () => import('./views/Users.vue');
const User = () => import('./views/User.vue');
const UserEdit = () => import('./views/UserEdit');


Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/users'
    },
    {
      path: '/users',
      name: 'users',
      component: Users
    },
    {
      path: '/users/:id',
      name: 'user-page',
      component: User
    },
    {
      path: '/users/:id/edit',
      name: 'user-edit',
      component: UserEdit
    },
    {
      path: '/albums',
      name: 'albums',
    },
    {
      path: '/albums/:id',
      name: 'album-page',
    },
    {
      path: '/albums/:id/edit',
      name: 'album-edit'
    }
  ]
})
