export default {
  addPhoto: (state, photo) => {
    state.photos = {
      ...state.photos,
      [photo.id]: photo
    }
  },

  deletePhoto: (state, id) => delete state.photos[id]
}