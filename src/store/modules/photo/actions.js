import {
  ADD_PHOTO,
  DELETE_PHOTO
} from './mutationTypes';

export default {
  addPhoto({ commit }, photoUrl) {
    commit(ADD_PHOTO, photoUrl);
  },

  deleteAlbum({ commit }, photoId) {
    commit(DELETE_PHOTO, photoId);
  },
}