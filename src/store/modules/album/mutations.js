import {
  SET_ALBUMS,
  SET_ALBUM,
  DELETE_ALBUM
} from './mutationTypes';
import { albumMapper } from "@/services/Mapper";

export default {
  [SET_ALBUMS]: (state, albums) => {
    state.albums = {
      ...state.albums,
      ...albums.reduce(
        (prev, album) => ({ ...prev, [album.id]: albumMapper(album) }),
        {}
      )
    }
  },

  [SET_ALBUM]: (state, album) => {
    state.albums = {
      ...state.albums,
      [album.id]: album
    }
  },

  [DELETE_ALBUM]: (state, id) => {
    delete state.albums[id]
  }
}