export default {
  albumsFilteredByUserName: state => userId => state.albums
    .filter(album => album.userId === userId)
}