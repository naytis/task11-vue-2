export const SET_ALBUMS = 'setAlbums';
export const SET_ALBUM = 'setAlbum';
export const DELETE_ALBUM = 'deleteAlbum';