import {
  SET_ALBUMS,
  SET_ALBUM,
  DELETE_ALBUM
} from './mutationTypes';
import api from '@/services/Api';
import { albumMapper } from "@/services/Mapper";

export default {
  async fetchAlbums({ commit }) {
    try {
      const albums = await api.get('/albums');

      commit(SET_ALBUMS, albums);

      return Promise.resolve(
        albums.map(albumMapper)
      );
    } catch (error) {
      return Promise.reject(error);
    }
  },

  async addAlbum({ commit }, album) {
    try {
      commit(SET_ALBUM, album);

      return Promise.resolve(albumMapper(album))
    } catch (error) {
      return Promise.reject(error);
    }
  },

  async updateAlbum({ commit }, album) {
    try {
      commit(SET_ALBUM, album);

      return Promise.resolve(albumMapper(album))
    } catch (error) {
      return Promise.reject(error);
    }
  },

  async deleteAlbum({ commit }, albumId) {
    try {
      commit(DELETE_ALBUM, albumId);

      return Promise.resolve()
    } catch (error) {
      return Promise.reject(error);
    }
  },
}