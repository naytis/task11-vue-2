import {
  SET_USERS,
  SET_USER,
  DELETE_USER
} from './mutationTypes';
import api from '@/services/Api';
import { userMapper } from "@/services/Mapper";

export default {
  async fetchUsers({ commit }) {
    try {
      const users = await api.get('/users');

      commit(SET_USERS, users);

      return Promise.resolve(
        users.map(userMapper)
      );
    } catch (error) {
      return Promise.reject(error);
    }
  },

  async fetchUserById({ commit }, id) {
    try {
      const user = await api.get('/users', { id });

      commit(SET_USER, ...user);

      return Promise.resolve(userMapper(...user));
    } catch (error) {
      return Promise.reject(error);
    }
  },

  async addUser({ commit }, user) {
    try {
      commit(SET_USER, user);

      return Promise.resolve(userMapper(user));
    } catch (error) {
      return Promise.reject(error);
    }
  },

  async updateUser({ commit }, user) {
    try {
      commit(SET_USER, user);

      return Promise.resolve(userMapper(user))
    } catch (error) {
      return Promise.reject(error);
    }
  },

  async deleteUser({ commit }, userId) {
    try {
      commit(DELETE_USER, userId);

      return Promise.resolve();
    } catch (error) {
      return Promise.reject(error);
    }
  }
}