export default {
  getFilteredUsers: state => (filter, filterBy) => Object.values(state.users)
    .filter(user => user[filterBy].toLowerCase().includes(filter.toLowerCase())),

  getUserById: state => id => state.users[id]
}