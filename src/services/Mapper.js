export const userMapper = user => ({
  id: user.id,
  name: user.name,
  email: user.email,
  avatar: ''
});

export const albumMapper = album => ({
  id: album.id,
  userId: album.userId,
  title: album.title,
  preview: ''
});